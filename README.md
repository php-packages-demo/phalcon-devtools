# phalcon-devtools

Scripts to generate code that use Phalcon framework. https://packagist.org/packages/phalcon/devtools

[![PHPPackages Rank](http://phppackages.org/p/phalcon/devtools/badge/rank.svg)](http://phppackages.org/p/phalcon/devtools)
[![PHPPackages Referenced By](http://phppackages.org/p/phalcon/devtools/badge/referenced-by.svg)](http://phppackages.org/p/phalcon/devtools)

## Unofficial documentation
* [*Create Project with Phalcon Developer Tools in Linux*
  ](https://tecadmin.net/creating-project-with-phalcon-developer-tools-in-linux/)
  2018-08 Rahul

### Tutorial
* [phalcon tutorial](https://www.google.com/search?q=phalcon+tutorial)
* [*Phalcon Tutorial*](https://www.tutorialspoint.com/phalcon/index.htm)

### Phalcon linux tutorial
* [phalcon linux tutorial](https://google.com/search?q=phalcon+linux+tutorial)
* [*The Complete Phalcon Tutorial for Beginners*
  ](https://www.hostinger.com/tutorials/phalcon-tutorial)
  2019-02 Gediminas B.
